from flask import Flask, render_template
from flask_serial import Serial
from flask_socketio import SocketIO, emit, send
from flask_cors import CORS, cross_origin
import datetime
import eventlet
import json

eventlet.monkey_patch()
app = Flask(__name__)
app.config['SERIAL_TIMEOUT'] = 1
app.config['SERIAL_PORT'] = 'COM1'
app.config['SERIAL_BAUDRATE'] = 19200
app.config['SERIAL_BYTESIZE'] = 8
app.config['SERIAL_PARITY'] = 'N'
app.config['SERIAL_STOPBITS'] = 1
app.config['CORS_HEADERS'] = 'Content-Type'

ise4000_data = ["pid", "pat", "b", "K", "Na", "Cl", "Ca", "pH", "Co2", "Date"]

currentDate = datetime.datetime.today().strftime("%Y-%m-%d")

instrumentInfo = {"Instrument": "ISE-4000"}

ser = Serial(app)
socketio = SocketIO(app)
cors = CORS(app)

@app.route('/ise4000')
@cross_origin()
def index():
    return "I am ISE-4000"

@ser.on_message()
def handle_message(msg):
    # incoming serial message = b'001  000000000000000000  000 4.98 135.1 100.8 0.00 0.00   6.6\r\n'
    # strip and split into array list
    serData = msg.decode('utf-8').strip().split()

    # appending current date
    serData.append(currentDate)

    dataDic = dict(zip(ise4000_data, serData))
    resultDic = Merge(dataDic, instrumentInfo)

    socketio.emit("serial_message", data={"message": json.dumps(resultDic)})

@ser.on_log()
def handle_logging(level, info):
    print(level, info)

@socketio.on('connect')
def test_connect():
    socketio.emit('connection response', {'data': 'Connected'})

# merging two dictionaries into one
def Merge(dict1, dict2):
    return {**dict1, **dict2}

if __name__ == '__main__':
    socketio.run(app, host='192.168.88.121', port=5000, debug=False)
